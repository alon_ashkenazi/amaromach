process.on('uncaughtException', (err) => {
    console.error('uncaughtException: ', err);
    process.exit(1);
}).on('unhandledRejection', (err) => {
    console.error('unhandledRejection: ', err);
    process.exit(1);
});

import {initConf} from '../utilities/init-conf';
initConf();
import nconf from "nconf";
import {app} from '../app';
import * as connector from '../db-handlers/connection/connector';
import {logger} from '../utilities/logger';
import * as wsConnector from '../ws-server/ws-connector';

const startApp = () => {
    const port = nconf.get('PORT');
    app.listen(port, () => {
        logger.info('Started the server on port: ' + port);
    });
};

const init = async () => {
    try {
        await connector.initDBConnection();
        await wsConnector.initWSConnection();
        startApp();
    } catch (err) {
        logger.error('Failed to start the server because ', err);
        process.exit(1);
    }
};

init();
