import * as DAL from "../db-handlers/dal/products-dal";
import {logger} from "../utilities/logger";
import {Socket} from "socket.io";
import {io} from "./ws-connector";
import {CartProduct} from "../types/cart-product";
import {joiValidateObjectWithSchema} from "../validations/joi-validator-object-with-schema";
import * as joiSchemas from "../validations/joi-validators-schemas";
import {MongooseProduct} from "../db-handlers/models/mongoose-product";
import {getCurrentConnectionBySocketId} from "./ws-state";


const getProducts = async (socket: Socket) => {
    const allProducts = await DAL.findAllProducts();
    socket.emit('products', allProducts);
};

const getSpecificProduct = async (id: string, socket: Socket) => {
    try {
        await joiValidateObjectWithSchema({id}, joiSchemas.objectIdValidationSchema);
        const product = await DAL.findSpecificProduct(id);
        socket.emit('product', product);
    } catch (err) {
        logger.error(`Could not find product with ${id} throughout websocket`, err);
    }
};

const createProduct = async (productToCreate: MongooseProduct) => {
    try {
        await joiValidateObjectWithSchema(productToCreate, joiSchemas.joiProductValidationSchema);
        const product = await DAL.createProduct(productToCreate);
        io.emit('product', product);
    } catch (err) {
        logger.error(`Could not create product ${JSON.stringify(productToCreate)} throughout websocket`, err);
    }
};

const updateProduct = async (productToUpdateById: { id: string; productToUpdate: MongooseProduct }) => {
    try {
        await joiValidateObjectWithSchema({id:productToUpdateById.id}, joiSchemas.objectIdValidationSchema);
        await joiValidateObjectWithSchema(productToUpdateById.productToUpdate, joiSchemas.joiProductValidationSchema);

        const product = await DAL.updateProduct(productToUpdateById.id, productToUpdateById.productToUpdate);
        io.emit('product', product);
    } catch (err) {
        logger.error(`Could not update product ${JSON.stringify(productToUpdateById.productToUpdate)} with id ${productToUpdateById.id} throughout websocket`, err);
    }
};

const deleteProduct = async (id: string) => {
    await joiValidateObjectWithSchema({id:id}, joiSchemas.objectIdValidationSchema);
    await DAL.deleteProduct(id);
    const products = await DAL.findAllProducts();
    io.emit('products', products);
};

const addToCart = async (socket: Socket, productIdAndAmountObjectOnCart: CartProduct) => {
    try {
        await joiValidateObjectWithSchema({id:productIdAndAmountObjectOnCart.id}, joiSchemas.objectIdValidationSchema);
        getCurrentConnectionBySocketId(socket.id).arrayOfItemsInCart.push(productIdAndAmountObjectOnCart);
        const product = await DAL.findSpecificProduct(productIdAndAmountObjectOnCart.id);
        product.amount -= productIdAndAmountObjectOnCart.amount;
        io.emit('product', product);
    } catch (err) {
        logger.error(`Could not add to cart product ${JSON.stringify(productIdAndAmountObjectOnCart)} throughout websocket`, err);
    }
};

const checkout = async (socket: Socket) => {
    try {
        await DAL.updateProductsAmount(getCurrentConnectionBySocketId(socket.id).arrayOfItemsInCart);
        getCurrentConnectionBySocketId(socket.id).arrayOfItemsInCart = [];
    } catch (err) {
        logger.error(`failed to checkout cart of socket with id ${JSON.stringify(socket.id)} throughout websocket`, err);
    }
};

export const initMyApiEvents = async (socket: Socket) => {
    socket.on('getProducts', async () => {
        await getProducts(socket);
    });

    socket.on('getSpecificProduct', async (id: string) => {
        await getSpecificProduct(id, socket);
    });

    socket.on('createProduct', async (productToCreate: MongooseProduct) => {
        await createProduct(productToCreate);
    });

    socket.on('updateProduct', async (productToUpdateById: { id: string, productToUpdate: MongooseProduct }) => {
        await updateProduct(productToUpdateById);
    });

    socket.on('deleteProduct', async (id: string) => {
        await deleteProduct(id);
    });

    socket.on('addToCart', async (productIdAndAmountObjectOnCart: CartProduct) => {
        await addToCart(socket, productIdAndAmountObjectOnCart);
    });

    socket.on('checkout', async () => {
        await checkout(socket);
    });
};