import * as DAL from "../db-handlers/dal/products-dal";
import {logger} from "../utilities/logger";
import {io} from "./ws-connector";
import {Socket} from "socket.io";
import {CartProduct} from "../types/cart-product";
import {getCurrentConnectionBySocketId, setCurrentConnectionBySocketId} from "./ws-state";

async function clearSocketCart(socket: Socket) {
    getCurrentConnectionBySocketId(socket.id).arrayOfItemsInCart.forEach(async (socketCartProduct: CartProduct) => {
        const product = await DAL.findSpecificProduct(socketCartProduct.id);
        io.emit('product', product);
    });
    setCurrentConnectionBySocketId(socket.id, {socket: socket, arrayOfItemsInCart: []});
}

export const initInternalEvents = async (socket: Socket) => {
    socket.on('disconnect', async () => {
        try {
            logger.debug(`disconnect socket id:  ${socket.id}`);
            await clearSocketCart(socket);
        } catch (err) {
            logger.error(`failed to disconnect socket with id ${JSON.stringify(socket.id)} throughout websocket`, err);
        }
    });

    socket.on('connect_error', async () => {
        logger.warn(`connection error with socket id: ${socket.id}`);
        try {
            await clearSocketCart(socket);
        } catch (err) {
            logger.error(`failed to clear socket cart, socket with id ${JSON.stringify(socket.id)} throughout websocket`, err);
        }
    });
};
