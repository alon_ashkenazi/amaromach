import {Socket} from "socket.io";
import {SocketConnectionState} from "./socket-connection-state";

const currentConnectionsMap: Map<string, SocketConnectionState> = new Map();

export const getCurrentConnectionBySocketId = (socketId: string) => {
    return currentConnectionsMap.get(socketId);
};

export const setCurrentConnectionBySocketId = (socketId: string, socketConnectionState: SocketConnectionState) => {
    currentConnectionsMap.set(socketId, socketConnectionState);
    return new Map(currentConnectionsMap);
};

export const setInitialStateOfSocket = async (socket: Socket) => {
    setCurrentConnectionBySocketId(socket.id, {socket: socket, arrayOfItemsInCart: []});
};