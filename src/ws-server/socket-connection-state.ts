import {Socket} from "socket.io";
import {CartProduct} from "../types/cart-product";

export interface SocketConnectionState {
    socket: Socket;
    arrayOfItemsInCart: Array<CartProduct>;
}
