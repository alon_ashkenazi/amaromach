import * as DAL from "../db-handlers/dal/products-dal";
import nconf from "nconf";
import {logger} from "../utilities/logger";
import socket, {Socket} from "socket.io";
import {setInitialStateOfSocket} from "./ws-state";
import {initMyApiEvents} from "./ws-my-events-api";
import {initInternalEvents} from "./ws-internal-events-api";

export const io = socket(nconf.get("WS_PORT"));

export const initWSConnection = async () => {

    io.on('connection', async (socket: Socket) => {
        try {
            await setInitialStateOfSocket(socket);
            await initMyApiEvents(socket);
            await initInternalEvents(socket);
            const allProducts = await DAL.findAllProducts();
            socket.emit('products', allProducts);
        } catch (err) {
            logger.warn(`Could not execute connection with socket`, err);
        }
    });

};
