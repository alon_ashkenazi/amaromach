import mongoose, {Document, model, Model, Schema} from 'mongoose';
import {Product} from "../../types/product";

export interface MongooseProduct extends Document, Product{}

export const ProductSchema: Schema = new mongoose.Schema({
    name: String,
    description: String,
    price: Number,
    amount: Number,
});

export const ProductModel: Model<MongooseProduct> = model<MongooseProduct>('products', ProductSchema);
