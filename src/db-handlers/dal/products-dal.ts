import HttpStatus from 'http-status-codes';
import {ApiError} from '../../utilities/api-error';
import {logger} from '../../utilities/logger';
import {ProductModel} from '../models/mongoose-product';
import {CartProduct} from "../../types/cart-product";
import {Product} from "../../types/product";

export const findAllProducts = async () => {
    try {
        return await (ProductModel.find({}));
    } catch (err) {
        logger.error('Could not get all products because ', err);
        throw new ApiError(`Could not get all products because: ${err}`, HttpStatus.NOT_FOUND);
    }
};

export const findSpecificProduct = async (id: string) => {
    try {
        return await (ProductModel.findById(id));
    } catch (err) {
        logger.error('Could not get product with id: ' + id, err);
        throw new ApiError(`Could not get product with id: ${id}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
};

export const createProduct = async (productObject: Product) => {
    try {
        const newProductToSave = new ProductModel(productObject);
        return await newProductToSave.save();
    } catch (err) {
        logger.error('Could not create product because', err);
        throw new ApiError(`Could not create product because ${err}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
};

export const updateProduct = async (id: string, update: Product) => {
    try {
        return await ProductModel.findByIdAndUpdate( id,
            { $set: update },
            {new: true},
            );
    } catch (err) {
        logger.error('Could not update product because: ', err);
        throw new ApiError(`Could not update product because ${err}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
};

export const deleteProduct = async (id: string) => {
    try {
        return await ProductModel.findByIdAndDelete(id);
    } catch (err) {
        logger.error('Failed to delete product because: ', err);
        throw new ApiError(`Failed to delete product because:  ${err}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
};

export const updateProductsAmount = async (arrayOfCartProducts: CartProduct[]) => {
    let checkoutObjectAfterUpdate;
    const promises = arrayOfCartProducts.map(async (productToUpdate) => {
            try {
                checkoutObjectAfterUpdate = await ProductModel.findByIdAndUpdate(productToUpdate.id,
                    {$inc: {amount: (-1) * productToUpdate.amount}}, {new: true});
                return ({ok: true, id: checkoutObjectAfterUpdate.id, amount: checkoutObjectAfterUpdate.amount});
            } catch (err) {
                logger.warn(`Failed to update one product amount: id: ${productToUpdate.id}, amount: ${productToUpdate.amount}`, err);
                return ({ok: false, id: productToUpdate.id, amount: productToUpdate.amount});
            }
        },
    );
    return Promise.all(promises);
};