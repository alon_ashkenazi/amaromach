// Import the mongoose module
import mongoose from 'mongoose';
import nconf from 'nconf';
import {logger} from '../../utilities/logger';

const options = {
    useNewUrlParser: true,
    reconnectInterval: nconf.get('RECONNECT_INTERVAL'),
};

const initMongoConnectionEvents = () => {
    mongoose.connection.on('connected', () => {
        logger.info('Connection Established');
    }).on('reconnected', () => {
        logger.info('Connection Reestablished');
    }).on('disconnected', () => {
        logger.error('Connection Disconnected');
    }).on('close', () => {
        logger.info('Connection Closed');
    }).on('error', (error) => {
        logger.error('Connection error', error);
    }).on('open', () => {
        logger.info('Connected to mongo db');
    });
};

export const initDBConnection = async () => {
    try {
        const mongoDB = nconf.get('MONGO_URI');
        await mongoose.connect(mongoDB, options);
        initMongoConnectionEvents();
    } catch (err) {
        logger.error('Could not connect to db ', err);
        process.exit(1);
    }
};
