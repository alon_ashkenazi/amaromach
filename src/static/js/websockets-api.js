$(function () {
    const socket = io('http://localhost:3001');

    socket.on('connect_failed', function() {
        document.write("Sorry, there seems to be an issue with the connection!");
    });

    $('#getAllProducts').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('getProducts');
    });

    $('#getSpecificProduct').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        console.log($('#getSpecificProductId')[0].value);
        socket.emit('getSpecificProduct', $('#getSpecificProductId')[0].value);

    });

    $('#createProduct').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('createProduct', {
            name: $('#createProductName')[0].value, description: $('#createProductDescription')[0].value,
            price: Number($('#createProductPrice')[0].value), amount: Number($('#createProductAmount')[0].value)
        });
    });

    $('#updateProduct').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('updateProduct',{id:$('#updateProductId')[0].value, productToUpdate:
                {
                    name: $('#updateProductNewName')[0].value,
                    description: $('#updateProductNewDescription')[0].value,
                    price: Number($('#updateProductNewPrice')[0].value),
                    amount: Number($('#updateProductNewAmount')[0].value)
                }
        }
);

    });

    $('#deleteProduct').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('deleteProduct', $('#deleteProductId')[0].value);

    });

    $('#addToCart').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('addToCart', {
            id: $('#addToCartProductId')[0].value,
            amount: Number($('#addToCartProductAmount')[0].value)
        });
    });

    $('#checkout').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('checkout');
    });

    socket.on('products', function (arrayOfProducts) {
        arrayOfProducts.forEach(product => {
            $('#messages').append($('<li>').text(JSON.stringify(product)));
        })
    });

    socket.on('product', function (product) {
        $('#messages').append($('<li>').text(JSON.stringify(product)));
    });

});
