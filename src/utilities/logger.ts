import nconf from 'nconf';
import * as winston from 'winston';

const logFilePath = nconf.get('LOG_FILE');
const errorLogFilePath = nconf.get('ERROR_LOG_FILE');
const errorStackTracerFormat = winston.format((info) => {
    if (info.meta && info.meta instanceof Error) {
        info.message = `${info.message} \n${info.meta.stack}`;
    }
    return info;
});

export const logger = winston.createLogger({
    level: 'debug',
    defaultMeta: { service: 'AmaRomach'  },
    format: winston.format.combine(
        winston.format.splat(), // Necessary to produce the 'meta' property
        winston.format.timestamp(),
        winston.format.prettyPrint(),
        errorStackTracerFormat(),
    ),
    transports: [
        new winston.transports.File({ filename: errorLogFilePath, level: 'error' }),
        new winston.transports.File({ filename: logFilePath }),
        new winston.transports.Console({
            level: 'info',
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.simple(),
            ),
        }),
    ],
});
