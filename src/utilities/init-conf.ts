import nconf from 'nconf';
export const initConf = () => {
    nconf.file({ file: process.env.NODE_ENV + '.json' });
};
