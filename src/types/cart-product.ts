export type CartProduct = {
    id: string;
    amount: number;
}
