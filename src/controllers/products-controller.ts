import HttpStatus from 'http-status-codes';
import {Context} from 'koa';
import * as DAL from '../db-handlers/dal/products-dal';
import {ApiError} from '../utilities/api-error';
import {logger} from '../utilities/logger';

export const findAllProducts = async (ctx: Context) => {
    const allProducts = await DAL.findAllProducts();
    ctx.status = HttpStatus.OK;
    ctx.body = allProducts;
};

export const findSpecificProduct = async (ctx: Context) => {
    const id = ctx.params.productId;
    const product = await DAL.findSpecificProduct(id);
    if (product) {
        logger.info(`Found product: ${JSON.stringify(product)}`);
        ctx.status = HttpStatus.OK;
        ctx.body = product;
    } else {
        logger.info('Could not get product with id: ' + id);
        ctx.status = HttpStatus.NOT_FOUND;
        ctx.body = 'Could not get product with id: ' + id;
    }
};

export const createProduct = async (ctx: Context) => {
    await DAL.createProduct(ctx.request.body);
    logger.info(`Created product: ${ctx.request.body}`);
    ctx.status = HttpStatus.CREATED;
    ctx.body = ctx.request.body;
};

export const updateProduct = async (ctx: Context) => {
    const update = ctx.request.body;
    const id = ctx.params.productId;
    const result = await DAL.updateProduct(id, update);
    if (!result) {
        logger.info(`Could not update product ${JSON.stringify(update)}. result: ${JSON.stringify(update)}`);
        ctx.status = HttpStatus.BAD_REQUEST;
        ctx.body = `Could not update product ${JSON.stringify(update)}. result: ${JSON.stringify(update)} `;
    } else {
        logger.info(`Updated product: ${JSON.stringify(result)}`);
        ctx.status = HttpStatus.OK;
        ctx.body = update;
    }
};

export const deleteProduct = async (ctx: Context) => {
    const id = ctx.params.productId;
    const result = await DAL.deleteProduct(id);
    if (!result) {
        logger.info(`Could not delete product with id ${id}. result: ${JSON.stringify(result)} `);
        ctx.status = HttpStatus.NOT_FOUND;
        ctx.body = `Could not delete product with id ${id}. result: ${JSON.stringify(result)} `;
    } else {
        ctx.status = HttpStatus.OK;
        ctx.body = result;
    }
};

export const executeTransaction = async (ctx: Context) => {
    try {
        const results = await DAL.updateProductsAmount(ctx.request.body);
        if (results.some((result) => !result.ok)) {
            ctx.status = HttpStatus.BAD_REQUEST;
            ctx.body = `checkout failed with results: ${JSON.stringify(results)}`;
        } else {
            logger.info('Checkout completed');
            ctx.status = HttpStatus.OK;
            ctx.body = results;
        }
    } catch (err) {
        logger.error('Could not checkout product because: ', err);
        throw new ApiError(`Failed to execute checkout ${err}`, HttpStatus.BAD_REQUEST);
    }
};
