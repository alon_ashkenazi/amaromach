import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static-server';
import nconf from 'nconf';
import {errorHandler} from './middlewares/error-handler';
import {logRequestMiddleware} from './middlewares/log-request-middleware';
import {router} from './routes/routes';

export const app = new Koa();

app.use(bodyParser());
app.use(errorHandler);
app.use(logRequestMiddleware);
app.use(router.routes());
app.use(router.allowedMethods());
app.use(serve({rootDir: nconf.get('STATIC_FILES_PATH')}));
