import Router from 'koa-router';
import * as productsController from '../controllers/products-controller';
export const router = new Router();
import {joiValidationIdMiddleware, joiValidationObjectMiddleware} from '../middlewares/joi-validation-middlewares';
import {createReadStream} from "fs";
router.param('productId', joiValidationIdMiddleware);

router.get('/products', productsController.findAllProducts);
router.get('/products/:productId',  productsController.findSpecificProduct);
router.post('/products', joiValidationObjectMiddleware, productsController.createProduct);
router.put('/products/:productId', joiValidationObjectMiddleware , productsController.updateProduct);
router.delete('/products/:productId', productsController.deleteProduct);
router.post('/checkout', productsController.executeTransaction);

router.get('/index', async (ctx) => {
    ctx.type = 'html';
    ctx.body = createReadStream('src/static/htmls/index.html');
});
