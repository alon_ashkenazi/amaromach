import HttpStatus from 'http-status-codes';
import {Schema} from 'Joi';
import {ObjectId} from '../db-handlers/models/object-id';
import {MongooseProduct} from '../db-handlers/models/mongoose-product';
import {ApiError} from '../utilities/api-error';
import {logger} from '../utilities/logger';

import Joi from 'joi';

export const joiValidateObjectWithSchema = async (objectToValidate: ObjectId | MongooseProduct, schema: Schema) => {
    try {
        await Joi.validate(objectToValidate, schema);
    } catch (err) {
        logger.warn(`Joi validation failure: `, err);
        throw new ApiError(`Joi validation failure: ${err}`, HttpStatus.NOT_FOUND);
    }
};
