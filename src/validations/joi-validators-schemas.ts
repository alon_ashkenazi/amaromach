import Joi = require('joi');

export const objectIdValidationSchema = Joi.object(
    {
        id: Joi.string().min(5).max(30).required(),
    });

export const joiProductValidationSchema = Joi.object(
    {
        name: Joi.string().min(2).max(30).required(),
        description: Joi.string().min(0).max(70),
        price: Joi.number().positive().required(),
        amount: Joi.number().integer().positive().required(),
    });
