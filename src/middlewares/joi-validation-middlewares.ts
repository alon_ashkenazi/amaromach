import {Context} from 'koa';
import {joiValidateObjectWithSchema} from '../validations/joi-validator-object-with-schema';
import * as joiSchemas from '../validations/joi-validators-schemas';

export const joiValidationObjectMiddleware = async (ctx: Context, next: () => Promise<any>) => {
    await joiValidateObjectWithSchema(ctx.params.body, joiSchemas.joiProductValidationSchema);
    await next();
};

export const joiValidationIdMiddleware = async (id: string, ctx: Context, next: () => Promise<any>) => {
    await joiValidateObjectWithSchema({id}, joiSchemas.objectIdValidationSchema);
    await next();
};
