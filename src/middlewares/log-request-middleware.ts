import {Context} from 'koa';
import {logger} from '../utilities/logger';

export const logRequestMiddleware = async (ctx: Context, next: () => Promise<any>) => {
    logger.info(`Method: ${ctx.method} url: ${ctx.url}`);
    const timeBeforeRequest = Date.now();
    await next();
    logger.debug(`Request took ${Date.now() - timeBeforeRequest} milliSeconds, resulted with response: ${ctx.response.body}`);
    logger.info(`resulted with response status: ${ctx.response.status}`);
};
