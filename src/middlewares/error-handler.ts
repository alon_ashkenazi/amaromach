import {Context} from 'koa';
import {ApiError} from '../utilities/api-error';
import {logger} from '../utilities/logger';

export const errorHandler = async (ctx: Context, next: () => Promise<any>) => {
    try {
        await next();
    } catch (err) {
        logger.error(`errorHandler caught error: ${err}`);
        if (err instanceof ApiError) {
            ctx.status = err.status;
        }
        if (process.env.NODE_ENV === 'dev') {
            ctx.body = err.message;
        } else {
            ctx.body = 'Internal server error, please contact for support...';
        }
    }
};
